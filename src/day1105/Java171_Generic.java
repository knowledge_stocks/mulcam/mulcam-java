package day1105;

/*
 * 1 와일드카드(?)를 제공하는 이유
 *   :컬렉션에 저장되는 요소들이 특정한 객체로 정해진 상태에서 다른 객체형은
 *    저장할 수 없는 상태였다. 하지만 때로는 기존과 같이 모든 객체들을 저장해야
 *    할때가 있다. 
 * 2 와일드 카드의 구성
 *   <?> : 모든 객체자료형에 대한 배치를 의미한다.
 *   <? super 객체자료형> : 명시된 객체자료형이나 
 *                객체자료형의 상위 객체들의 배치를 의미한다.
 *   <? extends 객체자료형> : 명시된 객체자료형이나
 *                객체자료형으로 부터 상속받은 하위객체들의 배치를 의미한다.
 * 3. 와일드 카드 제네릭은 메소드의 매개변수나 맴버 변수 등에 사용할 수 있다.             
 */

class Seoul {
	public void showYou() {
		System.out.println("Seoul");
	}
}

class Fruit extends Seoul {
	@Override
	public void showYou() {
		System.out.println("Fruit");
	};
}

class Apple extends Fruit {
	@Override
	public void showYou() {
		System.out.println("Apple");
	};
}

class FruitBox<T extends Seoul> {
	private T item;
	
	public FruitBox(T item) {
		this.item = item;
	}
	
	public void store(T item) {
		this.item = item;
	}
	
	public T pullout() {
		return item;
	}
}

public class Java171_Generic {

	public static void main(String[] args) {
		FruitBox<Apple> fApple = new FruitBox<Apple>(new Apple());
		openAndShowFruitBox(fApple);
		
		// Fruit의 super 클래스만 가능하기 때문에 Apple은 사용할 수 없다.
		// openAndShowFruitBox2(fApple);
	}
	
	public static void openAndShowFruitBox(FruitBox<? extends Fruit> params) {
		params.pullout().showYou();
	}
	
	// super 와일드 카드를 활용하는 예시로
	// List.sort의 Comparator<? super E>를 들 수 있다.
	// Camparator의 제네릭 타입은 List의 제네릭 타입의 부모 범위로 한정된다.
	public static void openAndShowFruitBox2(FruitBox<? super Fruit> params) {
		// Fruit의 super 클래스이기 때문에 반드시 showYou가 있다고 단정지을 수 없다.
		// 하지만 FruitBox의 제네릭 타입이 Seoul의 상속 클래스로 한정되어 있다면 showYou가 있다고 단정할 수 있다. 
		// FruitBox의 제네릭 타입에서 extends 부분을 지우면 아래 코드에서 오류가 발생한다.
		params.pullout().showYou();
	}
}
