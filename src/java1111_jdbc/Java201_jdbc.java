package java1111_jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Java201_jdbc {

	public static void main(String[] args) {
		String url = "jdbc:mysql://192.168.56.201:23306/shopdb?useUnicode=true&serverTimezone=UTC";
		String user = "root";
		String pass = "asdf1234";
		
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			
			conn = DriverManager.getConnection(url, user, pass);
			System.out.println("db접속완료:" + conn);
			
			stmt = conn.createStatement();
			
			rs = stmt.executeQuery("SELECT * FROM memberTBL");
			while(rs.next()) {
				System.out.printf("%s %s %s\n",
						rs.getNString("memberId"), rs.getNString("memberName"), rs.getNString("memberAddress"));
			}
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if(rs != null) {
					rs.close();
					rs = null;
				}
				if(stmt != null) {
					stmt.close();
					stmt = null;
				}
				if(conn != null) {
					conn.close();
					conn = null;
				}
			} catch(SQLException e) {
				e.printStackTrace();
			}
		}
	}

}
