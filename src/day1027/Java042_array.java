package day1027;

public class Java042_array {

	public static void main(String[] args) {
		// 다차원 배열일 경우 배열 크기를 가변으로 설정할 수 있다.
		int[][] array = new int[3][];
		
		array[0] = new int[2];
		array[1] = new int[3];
		array[2] = new int[4];
	}

}
