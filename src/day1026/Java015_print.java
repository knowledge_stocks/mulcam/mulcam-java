package day1026;

import java.text.MessageFormat;

public class Java015_print {

	public static void main(String[] args) {
		// printf 정수
		System.out.printf("%d\n", 5000);
		System.out.printf("%3d\n", 5000);
		System.out.printf("%5d\n", 5000);
		
		// printf 실수
		System.out.printf("%f\n", 1.5);
		System.out.printf("%.2f\n", 1.5);
		
		// MessageFormat 정수
		System.out.println(MessageFormat.format("{0}", 5000));
		System.out.println(MessageFormat.format("{0,number,#}", 5000));
		System.out.println(MessageFormat.format("{0,number,currency}", 5000));
		System.out.println(MessageFormat.format("{0,number,percent}", 5000));
		
		// MessageFormat 실수
		System.out.println(MessageFormat.format("{0}", 1.544));
		System.out.println(MessageFormat.format("{0,number,#}", 1.544));
		System.out.println(MessageFormat.format("{0,number,#.##}", 1.544));
	}

}
