package day1026.prob.prob02;

/*
for문을 이용해서 12의 약수를 구하시오
약수 : 어떤 수를 나누어서 0으로 떨어진 수
1
2 
3 
4 
6 
12
*/

public class Prob_01 {

	public static void main(String[] args) {
		int num = 12;
		
		System.out.println(1);
		for(int test = 2; test <= num / 2; test++) {
			if(num % test == 0) {
				System.out.println(test);
			}
		}
		System.out.println(num);

	}//end main()

}//end class













