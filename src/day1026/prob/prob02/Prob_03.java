package day1026.prob.prob02;

/*
1+
1+2+
1+2+3+
1+2+3+4+
1+2+3+4+5=??

출력결과
sum=35
*/
public class Prob_03 {

	public static void main(String[] args) {
		int numTo = 5;
		int sum = 0;
		
		for(int num = 1; num <= numTo; num++) {
			sum += num;
		}
		
		System.out.println("sum=" + sum);
	}// end main()

}// end class
