package day1103;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

/*
 * 2020년 2월 마지막일과 요일을 구하는 프로그램을 구현하세요.
 * [출력결과]
 * 2020-2-29 토요일
 */
public class Java143_Calendar {

	public static void main(String[] args) {
		Calendar cal = Calendar.getInstance();
		
		cal.set(2020, 1, 1);
		cal.set(Calendar.DAY_OF_MONTH, cal.getActualMaximum(Calendar.DAY_OF_MONTH));
		
		// Calendar.getTimeInMillis와 System.currentTimeMillis의 결과는 타임존에 영향을 받지 않는다.
		System.out.println(new SimpleDateFormat("yyyy-M-dd EEEE", Locale.KOREA).format(cal.getTimeInMillis()));
	}

}
