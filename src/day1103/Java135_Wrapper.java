package day1103;

/*
 * 1 기본자료형을 클래스로 정의해놓은 것을 Wrapper라한다.
 *   char -> Character
 *   byte	-> Byte
 *   short	-> Short
 *   int	-> Integer
 *   long	-> Long
 *   float	-> Float
 *   double	-> Double
 *   boolean -> Boolean
 *
 * 2 메모리에 저장된 값을 다른 자료형으로 변환해주는 메소드를 제공하기 위해서
 *   Wrapper클래스를 제공한다.
 * 
 * 3 auto boxing과 auto unboxing은 jdk5.0에서 추가된 기능이다.
 *   auto boxing => 스택 -> 힙영역에 복사
 *   auto unboxing => 힙 -> 스택영역에 복사
 */
public class Java135_Wrapper {

	public static void main(String[] args) {
		//String to Integer = auto boxing?
		Integer it = 1234;
		
		//Integer to int = unboxing
		int num = it.intValue();
		
		// auto unboxing
		int num2 = it;
	}

}
