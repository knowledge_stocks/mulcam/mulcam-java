package day1103.prob;

/*
 * [출력결과]
 *  영어 소문자 갯수:3
 *  영어 대문자 갯수:3
 *  숫자 갯수:2
 */

public class Prob007_String {

	public static void main(String[] args) {
		String data = "ke4RdTA5";

		display(process(data));
	}// end main()

	public static char[] process(String data) {
		return data.toCharArray();
	}// end process()

	public static void display(char[] arr) {
		int lowerCount = 0;
		int uppderCount = 0;
		int numCount = 0;
		
		for (Character c : arr) {
			if(Character.isLowerCase(c)) {
				lowerCount++;
			} else if(Character.isUpperCase(c)) {
				uppderCount++;
			}
			if(Character.isDigit(c)) {
				numCount++;
			}
		}
		
		System.out.println("영어 소문자 갯수:" + lowerCount);
		System.out.println("영어 대문자 갯수:" + uppderCount);
		System.out.println("숫자 갯수:" + numCount);
	}// end display()

}// end class
