package day1103;

public class Java141_System {

	public static void main(String[] args) {
		int[] arr1 = {1,5,2,4};
		int[] arr2 = {6,3,9,7,8};
		int[] arr3 = {4,2};
		
		int[] result = concatArr(arr1, arr2, arr3);
		for(int v : result) {
			System.out.println(v);
		}
	}
	
	public static int[] concatArr(int[] arr1, int[] arr2, int[] arr3) {
		int[] arr = new int[arr1.length + arr2.length + arr3.length];
		System.arraycopy(arr1, 0, arr, 0, arr1.length);
		System.arraycopy(arr2, 0, arr, arr1.length, arr2.length);
		System.arraycopy(arr3, 0, arr, arr1.length + arr2.length, arr3.length);
		
		return arr;
	}
}
