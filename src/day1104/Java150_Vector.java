package day1104;

import java.util.Vector;

public class Java150_Vector {

	public static void main(String[] args) {
		Vector v = new Vector();
		
		System.out.println("벡터의 기본 capacity = " + v.capacity());
		
		int oldCapacity = v.capacity();
		for(int i = 0; i < oldCapacity + 1; i++) {
			v.add(i);
		}
		int newCapacity = v.capacity();
		
		System.out.println("늘어난 벡터의 capacity = " + newCapacity);
		
		System.out.println("벡터의 기본 growth rate = " + (double)newCapacity / oldCapacity);
		
		v.clear();
		System.out.println("clear() 후 벡터의 capacity = " + v.capacity());
		
		// Vector의 기본 Growth rate가 2인 이유는 일단 메모리 영역이 어떻게 확장되는지 알아야한다.
		//
		// capacity를 초과하는 add가 발생해서 공간을 확장해야 할 때
		// 메모리 공간의 특성상 기존 공간을 학장시키는 것이 위험하기 때문에
		// 기존보다 큰 새로운 공간을 할당하고 데이터를 복사한 뒤 기존 공간은 삭제하는 식으로 확장이 이루어진다.
		// 
		// 따라서 메모리 확장 작업이 너무 자주 발생하거나, 한번에 확장되는 크기가 너무 클 경우 작업에 들어가는 시간적 비용이 많이 들게 된다.
		// 이를 계산한 결과 Growth rate가 2.25일 때 가장 낮은 비용이 발생하기 때문에, Vector의 기본 Growth rate는 2로 설정되어 있다.
		// ArrayList의 경우 1.5로 설정되어 있는데, 매번 동기화가 필요한 Vector에 비해 비용이 적게 들어가기 때문이 아닐까 싶다.
		//
		// 출처
		// https://m.blog.naver.com/PostView.naver?isHttpsRedirect=true&blogId=chogahui05&logNo=221496448248
		
		
		System.out.println();
		System.out.println("벡터의 처음 크기와, 증가량을 각각 2, 3으로 지정한 경우");
		
		v = new Vector(2, 3);
		
		System.out.println("벡터의 기본 capacity = " + v.capacity());
		
		oldCapacity = v.capacity();
		for(int i = 0; i < oldCapacity + 1; i++) {
			v.add(i);
		}
		newCapacity = v.capacity();
		
		System.out.println("늘어난 벡터의 capacity = " + newCapacity);
	}

}
