package day1104.prob;

import java.util.ArrayList;


public class BookManager {
	public static int getRentalPrice(ArrayList<BookDTO> bookList, String kind) {
		// 구현하세요.
		int price = 0;
		for (BookDTO bookDTO : bookList) {
			if(bookDTO.getKind().equals(kind)) {
				price += bookDTO.getRentalPrice();
			}
		}
		
		return price;
	}//end getRentalPrice()
}//end class






