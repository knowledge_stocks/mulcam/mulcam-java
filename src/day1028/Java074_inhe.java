package day1028;

public class Java074_inhe {
	public static void main(String[] args) {
		Child c = new Child();
		c.printA();
		
		((Parent)c).printA();
	}
}

class Parent {
	int a = 1;
	
	void printA() {
		System.out.println(a);
		
		// 어떻게 해봐도 자식에서 재정의된 함수를 호출한다.
		((Parent)this).printSome();
	}
	
	void printSome() {
		System.out.println("parent");
	}
}

class Child extends Parent {
	int a = 3;
	
	@Override
	void printA() {
		super.printA();
		System.out.println(a);
	}
	
	@Override
	void printSome() {
		System.out.println("child");
	}
}