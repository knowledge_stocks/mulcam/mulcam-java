package java1108_thread;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.Iterator;
import java.util.Vector;

public class Java200_ChatHandler implements Runnable {
	private Socket socket;
	private DataInputStream dataIn;
	private DataOutputStream dataOut;
	private Thread th;
	private static Vector<Java200_ChatHandler> userVec = new Vector<Java200_ChatHandler>();
	
	public Java200_ChatHandler(Socket socket) {
		this.socket = socket;
	}
	
	public void initStart() {
		if(th == null) {
			try {
				InputStream is = socket.getInputStream();
				OutputStream os = socket.getOutputStream();
				dataIn = new DataInputStream(new BufferedInputStream(is));
				dataOut = new DataOutputStream(new BufferedOutputStream(os));
				
				th = new Thread(this);
				th.start();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void run() {
		userVec.addElement(this);
		while(!Thread.interrupted()) {
			try {
				String message = dataIn.readUTF();
				broadcast(message);
			} catch (IOException e) {
				e.printStackTrace();
				System.out.println(socket.getInetAddress().getHostAddress() + "님이 나갔습니다.");
				stop();
				break;
			}
		}
	}
	
	private synchronized void stop() {
		if (th != null) {
			if (th != Thread.currentThread()) {
				th.interrupt();
				th = null;
				try {
					dataIn.close();
					dataOut.close();
					
				} catch (IOException e) {
					System.out.println(e.getMessage());
				} finally {
				}
			}
		}
	}
	
	public synchronized void broadcast(String message) {
		Iterator<Java200_ChatHandler> iter = userVec.iterator();
		while(iter.hasNext()) {
			Java200_ChatHandler handler = iter.next();
			try {
				handler.dataOut.writeUTF(message);
				handler.dataOut.flush();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
