package java1108_thread;

class TestThread extends Thread {
	@Override
	public void run() {
		for(int i = 0; i < 100; i++) {
			System.out.printf("%s : %d\n", getName(), i);
		}
	}
}

class TestRunnable implements Runnable {
	@Override
	public void run() {
		for(int i = 0; i < 100; i++) {
			System.out.printf("%s : %d\n", Thread.currentThread().getName(), i);
		}
	}
}

public class Java191_thread {

	public static void main(String[] args) {
//		new TestThread().run(); // 이렇게 실행하면 순차실행 됨
//
//		new TestThread().start(); // 이렇게 실행해야 비동기 실행 됨
		
		new Thread(new TestRunnable()).start(); // Runnable은 이렇게
		
		for(int i = 0; i < 100; i++) {
			System.out.printf("%s : %d\n", Thread.currentThread().getName(), i);
		}
	}
}
