package java1108_thread;

class ThreadEx1 extends Thread {
	@Override
	public void run() {
		for(int i = 0; i < 300; i++) {
			System.out.print("-");
			for(int x = 0; x < 100000000; x++);
		}
	}
}

class ThreadEx2 extends Thread {
	@Override
	public void run() {
		for(int i = 0; i < 300; i++) {
			System.out.print("|");
			for(int x = 0; x < 100000000; x++);
		}
	}
}

public class Java192_priority {
	public static void main(String[] args) {
		ThreadEx1 th1 = new ThreadEx1();
		ThreadEx2 th2 = new ThreadEx2();
		
		th2.setPriority(7);
		
		th1.start();
		th2.start();
	}

}
