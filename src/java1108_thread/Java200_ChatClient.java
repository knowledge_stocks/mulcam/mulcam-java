package java1108_thread;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class Java200_ChatClient implements Runnable, ActionListener {
	private String host;
	private int port;
	private String username;
	private JFrame frm;
	private JPanel pan;
	private JTextArea taOutput;
	private JLabel lbName;
	private JTextField tfInput;
	private DataInputStream dataIn;
	private DataOutputStream dataOut;
	Thread th;
	
	public Java200_ChatClient(String host, int port) {
		this.host = host;
		this.port = port;
		
		System.out.print("사용자 이름을 입력하세요: ");
		Scanner sc = new Scanner(System.in);
		username = sc.nextLine();
		if(username.isEmpty()) {
			username = "guest";
		}
		sc.close();
		
		System.out.println(username);
		
		initComponent();
	}
	
	private void initComponent() {
		frm = new JFrame("채팅 프로그램[" + host + ":" + port + "]");
		taOutput = new JTextArea();
		tfInput = new JTextField(10);
		pan = new JPanel();
		lbName = new JLabel("입력:");
		JScrollPane scroll = new JScrollPane(taOutput); // 출력 내용을 스크롤하기 위해
		
		frm.setSize(400, 400);
		frm.setVisible(true);
		frm.requestFocus();
		frm.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		taOutput.setEditable(false);

//		frm.add(BorderLayout.CENTER, taOutput); // 출력 내용이 스크롤되지 않음
		frm.add(BorderLayout.CENTER, scroll);
		frm.add(BorderLayout.SOUTH, pan);
		pan.add(lbName);
		pan.add(tfInput);
		
		frm.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				if(JOptionPane.showConfirmDialog(
						frm, "정말로 종료하겠습니까?", "종료", JOptionPane.YES_NO_OPTION)
						== JOptionPane.YES_OPTION) {
					stop();
				}
			}
		});
		tfInput.addActionListener(this);
	}
	
	private void initStart() {
		Socket socket = null;
		
		try {
			socket = new Socket(host, port);
			InputStream is = socket.getInputStream();
			OutputStream os = socket.getOutputStream();
			dataIn = new DataInputStream(new BufferedInputStream(is));
			dataOut = new DataOutputStream(new BufferedOutputStream(os));
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		th = new Thread(this);
		th.start();
	}
	
	@Override
	public void run() {
		while(!Thread.interrupted()) {
			try {
				String line = dataIn.readUTF();
				taOutput.append(line + "\r\n");
			} catch (IOException e) {
				System.out.println("연결이 종료되었습니다.");
			}
		}
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		String message = username + " : " + e.getActionCommand();
		
		try {
			dataOut.writeUTF(message);
			dataOut.flush();
			
			tfInput.setText("");
			tfInput.requestFocus();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}
	
	private void stop() {
		if (th != null) {
			th.interrupt();
			th = null;
		}
//		try {
//			dataIn.close();
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//		try {
//			dataOut.close();
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
		
		frm.setVisible(false);
		frm.dispose();
		System.exit(0);
	}
	
	public static void main(String[] args) {
		Java200_ChatClient client = new Java200_ChatClient("127.0.0.1", 7777);
		client.initStart();
	}
}
