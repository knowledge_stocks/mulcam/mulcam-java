package ncs.test13;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Inventory {
//	- productName:String //상품명
//	- putDate:Date //입고일
//	- getDate:Date //출고일
//	- putAmount:int //입고수량
//	- getAmount:int //출고수량
//	- inventoryAmount:int //재고수량

	private String productName;
	private Date putDate;
	private Date getDate;
	private int putAmount;
	private int getAmount;
	private int inventoryAmount;

	public Inventory() {
		super();
	}

	public Inventory(String productName, Date putDate, int putAmount) {
		this.productName = productName;
		this.putDate = putDate;
		this.putAmount = putAmount;
		this.inventoryAmount = putAmount;
	}

	@Override
	public String toString() {
		// 삼성 갤럭시S7, 2016년 3월 15일 입고, 30개, 2016년 4월 28일 출고, 10개, 재고 20개
		SimpleDateFormat format = new SimpleDateFormat("yyyy년 M월 d일");
		return productName + ", " + format.format(putDate) + " 입고, " + putAmount + "개, "
				+ (getDate == null ? "null, " : format.format(getDate) + " 출고, ") + getAmount + "개, " + "재고 "
				+ inventoryAmount + "개";
	}

	public String getProductName() {
		return productName;
	}

	public Date getPutDate() {
		return putDate;
	}

	public Date getGetDate() {
		return getDate;
	}

	public int getPutAmount() {
		return putAmount;
	}

	public int getGetAmount() {
		return getAmount;
	}

	public int getInventoryAmount() {
		return inventoryAmount;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public void setPutDate(Date putDate) {
		this.putDate = putDate;
	}

	public void setPutAmount(int putAmount) {
		this.putAmount = putAmount;
		inventoryAmount = putAmount - getAmount;
	}

	public void setGetAmount(int getAmount) throws Exception {
		if(getAmount > putAmount) {
			throw new Exception("입고량보다 출고량이 많을 수 없습니다.");
		}
		
		this.getAmount = getAmount;
		inventoryAmount = putAmount - getAmount;
		getDate = new Date();
	}
}
