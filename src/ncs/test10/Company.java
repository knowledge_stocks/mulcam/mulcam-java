package ncs.test10;

public class Company {

	public static void main(String[] args) {
		Employee[] employees = new Employee[2];
		
		// 1번의 사용 데이터를 기반으로 객체를 생성하여 배열에 넣는다 .
		employees[0] = new Secretary("Hilery", 1, "secretary", 800);
		employees[1] = new Sales("Clinten", 2, "sales", 1200);
		
		// 모든 객체의 기본 정보를 출력한다 (for 문을 이용하여 출력한다.)
		System.out.println("name\tdepartment\tsalary");
		System.out.println("------------------------------------");
		for (Employee employee : employees) {
			System.out.printf("%s\t%10s\t%d\n",
					employee.getName(), employee.getDepartment(), employee.getSalary());			
		}
		System.out.println();
		
		// 모든 객체에 인센티브 100 씩 지급한 급여를 계산하고 다시 객체의 salary에 넣는다 .
		System.out.println("인센티브 100 지급");
		for (Employee employee : employees) {
			if(employee instanceof Bonus) {
				((Bonus)employee).incentive(100);
			}
		}
		System.out.println();
		
		// 모든 객체의 정보와 세금을 출력한다 (for 문을 이용하여 출력한다.)
		System.out.println("name\tdepartment\tsalary\ttax");
		System.out.println("------------------------------------");
		for (Employee employee : employees) {
			System.out.printf("%s\t%10s\t%d\t%.1f\n",
					employee.getName(), employee.getDepartment(), employee.getSalary(), employee.tax());			
		}
	}

}
