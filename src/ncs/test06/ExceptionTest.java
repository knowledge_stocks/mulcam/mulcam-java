package ncs.test06;

import java.util.InputMismatchException;
import java.util.Scanner;

public class ExceptionTest {

	public static void main(String[] args) {
		try (Scanner scanner = new Scanner(System.in)) {
			System.out.print("2부터 5까지의 정수 입력: ");
			int data = scanner.nextInt();
			
			double result = Calculator.getSum(data);
			
			System.out.printf("결과값 : %.1f\n", result);
		} catch (InvalidException e) {
			System.out.println(e.getMessage());
		} catch (InputMismatchException e) {
			e.printStackTrace();
		}
	}

}
