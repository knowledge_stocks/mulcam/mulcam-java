package ncs.test06;

public class Calculator {
	public static double getSum(int data) throws InvalidException {
		if(data < 2 || data > 5) {
			throw new InvalidException("입력 값에 오류가 있습니다.");
		}
		
		double sum = 0;
		for(int n = 1; n <= data; n++) {
			sum += n;
		}
		
		return sum;
	}
}
