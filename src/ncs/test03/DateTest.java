package ncs.test03;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class DateTest {

	public static void main(String[] args) {
		int birthYear = 1987;
		int birthMonth = 5;
		int birthDate = 27;
		
		Calendar birthCal = Calendar.getInstance();
		Calendar now = Calendar.getInstance();
		
		birthCal.set(birthYear, birthMonth - 1, birthDate, 0, 0, 0);
		birthCal.set(Calendar.MILLISECOND, 0);

		System.out.println("생일 : "
				+ new SimpleDateFormat("y년 M월 d일 EEEE", Locale.KOREA)
					.format(birthCal.getTimeInMillis()));
		System.out.println("현재 : "
				+ new SimpleDateFormat("y년 M월 d일", Locale.KOREA)
					.format(now.getTimeInMillis()));
		System.out.println("나이 : "
				+ (now.get(Calendar.YEAR) - birthCal.get(Calendar.YEAR)));
	}

}
