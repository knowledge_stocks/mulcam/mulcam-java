package ncs.test12;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Properties;

public class PropTest {

	public static void main(String[] args) {
		Properties prop = new Properties();

		// 명시한 사용 데이터를 prop 에 기록한다.
		String propStr = "1=apple,1200,3, 2=banana,2500,2, 3=grape,4500,5, 4=orange,800,10, 5=melon,5000,2";

		for (String property : propStr.split(" ")) {
			String[] propParts = property.split("=");
			prop.setProperty(propParts[0], propParts[1]);
		}

		// fileSave() 메소드를 호출한다.
		fileSave(prop);

		// fileOpen() 메소드를 호출한다.
		fileOpen(prop);
	}

	public static void fileSave(Properties p) {
		File file = new File(".\\bin\\ncs\\test12\\data.xml");

		try (FileWriter writer = new FileWriter(file)) {
			p.store(writer, "data");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	// 뭘 원하는건지 모르겠다
	public static void fileOpen(Properties p) {
		File file = new File(".\\bin\\ncs\\test12\\data.xml");

		try (FileReader reader = new FileReader(file)) {
			p.load(reader);

			List<String> keyList = new ArrayList<String>();
			for (Object key : p.keySet()) {
				keyList.add((String) key);
			}
			Collections.sort(keyList);
			
			Fruit[] fruits = new Fruit[keyList.size()];
			for (int i = 0; i < fruits.length; i ++) {
				String[] parts = p.getProperty(keyList.get(i)).split(",");
				fruits[i] = new Fruit(parts[0], Integer.parseInt(parts[1]), Integer.parseInt(parts[2]));
			}
			
			int num = 1;
			for (Fruit fruit : fruits) {
				System.out.println(num++ + " = " + fruit);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
