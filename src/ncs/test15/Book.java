package ncs.test15;

public class Book {
//	- title:String
//	- author:String
//	- price:int
//	- publisher:String
//	- discountRate:double

	private String title;
	private String author;
	private int price;
	private String publisher;
	private double discountRate;

	public Book() {
		super();
	}

	public Book(String title, String author, int price, String publisher, double discountRate) {
		this.title = title;
		this.author = author;
		this.price = price;
		this.publisher = publisher;
		this.discountRate = discountRate;
	}

	@Override
	public String toString() {
		// 자바의 정석, 남궁성, 도우출판, 30000원, 15% 할인
		return title + ", " + author + ", " + publisher + ", " + price + "원, " + (int)(discountRate * 100) + "% 할인";
	}

	public String getTitle() {
		return title;
	}

	public String getAuthor() {
		return author;
	}

	public int getPrice() {
		return price;
	}

	public String getPublisher() {
		return publisher;
	}

	public double getDiscountRate() {
		return discountRate;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}

	public void setDiscountRate(double discountRate) {
		this.discountRate = discountRate;
	}
}
