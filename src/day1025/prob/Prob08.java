package day1025.prob;
/*
* 동전교환프로그램
* 1 가장 적은 동전으로 교환할 수 있는 프로그램작성하시오
* 2 [출력결과]
*   500원: 15개 
    100원: 2개
    50원: 1개
    10원: 2개
    1원: 7개
    */

public class Prob08 {

	public static void main(String[] args) {
		int money = 7777;
		int mok=money/500;
		
		//여기에 작성하시오.
		int countOf500 = money / 500;
		money %= 500;
		int countOf100 = money / 100;
		money %= 100;
		int countOf50 = money / 50;
		money %= 50;
		int countOf10 = money / 10;
		money %= 10;
		int countOf1 = money;
		
		System.out.println("500원: " + countOf500 + "개");
		System.out.println("100원: " + countOf100 + "개");
		System.out.println("50원: " + countOf50 + "개");
		System.out.println("10원: " + countOf10 + "개");
		System.out.println("1원: " + countOf1 + "개");
		
	}//end main()

}//end class


