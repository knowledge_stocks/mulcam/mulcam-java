package day1025;

public class Sample {

	public static void main(String[] args) {
		System.out.println("Hello Java!!!");
		
		// primitive 타입의 값들 타입 확인하기
		System.out.println(((Object)1).getClass().getName());
		System.out.println(((Object)1l).getClass().getName());
		System.out.println(((Object)(1f / 10.0)).getClass().getName());
		
		int x = 1;
		int y = 2;
		int z = 3;
		
		boolean res = x < y || ++x == y && ++z > y;
		System.out.println(x);
		System.out.println(y);
		System.out.println(z);
		System.out.println(res);
	}

}
