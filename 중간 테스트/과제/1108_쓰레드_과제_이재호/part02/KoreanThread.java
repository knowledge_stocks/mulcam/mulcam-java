package java1108_thread.prob.part02;

public class KoreanThread extends Thread {
	private final static char[] characters = "ㄱㄴㄷㄹㅁㅂㅅㅇㅈㅊㅋㅌㅍㅎ".toCharArray();

	@Override
	public void run() {
		for (char c : characters) {
			System.out.print(c);
		}
	}
}