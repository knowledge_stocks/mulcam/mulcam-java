package java1108_thread.prob.part02;

public class EnglishLowerThread extends Thread {
	private final static char[] characters = "abcdefghijklmnopqrstuvwxyz".toCharArray();

	@Override
	public void run() {
		for (char c : characters) {
			System.out.print(c);
		}
	}
}
