package java1108_thread.prob.part02;

public class EnglishUpperThread extends Thread {
	private final static char[] characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".toCharArray();

	@Override
	public void run() {
		for (char c : characters) {
			System.out.print(c);
		}
	}
}