package ncs.test06;

public class InvalidException extends Exception{
	
	public InvalidException(){}
	public InvalidException(String message){
		super(message);
	}
}
