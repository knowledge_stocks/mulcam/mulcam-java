package java1105_stream.prob;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.Vector;

/*
 * [문제] : booklist.txt 파일의 데이터를 이용하여 
 *      책 정보 하나당 하나의 BookShop 객체를 생성하고 생성된 BookShop 객체들을 
 *      Vector에 담아서 리턴하는 makeBookList() 메서드를 구현하시오.
 *      
 * [실행결과]
 * Java Programming 의 가격 : 25000
 * SQL Fundamentals 의 가격 : 47000
 * JDBC Programming 의 가격 : 30000
 * Servlet Programming 의 가격 : 20000
 * JSP Programming 의 가격 : 21000
 */
public class Prob006_Vector {

	public static void main(String[] args) throws Exception {
		Vector<BookShop> bookList = makeBookList();
		for (BookShop book : bookList) {
			System.out.println(book.getTitle() + " 의 가격 : " + book.getPrice());
		}

	}// end main()

	private static Vector<BookShop> makeBookList() throws Exception {
		// booklist.txt 파일의 데이터를 Vector에 저장한 후 리턴하는 프로그램을 구현하시오.
		Vector<BookShop> result = new Vector<BookShop>();
		
		File file = new File(".\\\\src\\\\java1105_stream\\\\prob\\\\booklist.txt");

		try (Scanner scanner = new Scanner(file)) {
			while (scanner.hasNext()) {
				String line = scanner.nextLine();

				String[] parts = line.split("/");
				
				result.add(new BookShop(parts[0], parts[1], parts[2], parts[3]));
			}
		} catch (FileNotFoundException e) {
			System.out.println("파일을 찾을 수 없습니다.");
			e.printStackTrace();
		}
		
		return result;

	}// end makeBookList()

}// end class















