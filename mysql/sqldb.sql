# 2021-11-12

DROP DATABASE IF EXISTS sqldb; -- 만약 sqldb가 존재하면 우선 삭제한다.
CREATE DATABASE sqldb;

USE sqldb;
CREATE TABLE usertbl -- 회원 테이블
( userID  	CHAR(8) NOT NULL PRIMARY KEY, -- 사용자 아이디(PK)
  name    	VARCHAR(10) NOT NULL, -- 이름
  birthYear   INT NOT NULL,  -- 출생년도
  addr	  	CHAR(2) NOT NULL, -- 지역(경기,서울,경남 식으로 2글자만입력)
  mobile1	CHAR(3), -- 휴대폰의 국번(011, 016, 017, 018, 019, 010 등)
  mobile2	CHAR(8), -- 휴대폰의 나머지 전화번호(하이픈제외)
  height    	SMALLINT,  -- 키
  mDate    	DATE  -- 회원 가입일
);
CREATE TABLE buytbl -- 회원 구매 테이블(Buy Table의 약자)
(  num 		INT AUTO_INCREMENT NOT NULL PRIMARY KEY, -- 순번(PK)
   userID  	CHAR(8) NOT NULL, -- 아이디(FK)
   prodName 	CHAR(6) NOT NULL, --  물품명
   groupName 	CHAR(4)  , -- 분류
   price     	INT  NOT NULL, -- 단가
   amount    	SMALLINT  NOT NULL, -- 수량
   FOREIGN KEY (userID) REFERENCES usertbl(userID)
);

INSERT INTO usertbl VALUES('LSG', '이승기', 1987, '서울', '011', '1111111', 182, '2008-8-8');
INSERT INTO usertbl VALUES('KBS', '김범수', 1979, '경남', '011', '2222222', 173, '2012-4-4');
INSERT INTO usertbl VALUES('KKH', '김경호', 1971, '전남', '019', '3333333', 177, '2007-7-7');
INSERT INTO usertbl VALUES('JYP', '조용필', 1950, '경기', '011', '4444444', 166, '2009-4-4');
INSERT INTO usertbl VALUES('SSK', '성시경', 1979, '서울', NULL  , NULL      , 186, '2013-12-12');
INSERT INTO usertbl VALUES('LJB', '임재범', 1963, '서울', '016', '6666666', 182, '2009-9-9');
INSERT INTO usertbl VALUES('YJS', '윤종신', 1969, '경남', NULL  , NULL      , 170, '2005-5-5');
INSERT INTO usertbl VALUES('EJW', '은지원', 1972, '경북', '011', '8888888', 174, '2014-3-3');
INSERT INTO usertbl VALUES('JKW', '조관우', 1965, '경기', '018', '9999999', 172, '2010-10-10');
INSERT INTO usertbl VALUES('BBK', '바비킴', 1973, '서울', '010', '0000000', 176, '2013-5-5');
INSERT INTO buytbl VALUES(NULL, 'KBS', '운동화', NULL   , 30,   2);
INSERT INTO buytbl VALUES(NULL, 'KBS', '노트북', '전자', 1000, 1);
INSERT INTO buytbl VALUES(NULL, 'JYP', '모니터', '전자', 200,  1);
INSERT INTO buytbl VALUES(NULL, 'BBK', '모니터', '전자', 200,  5);
INSERT INTO buytbl VALUES(NULL, 'KBS', '청바지', '의류', 50,   3);
INSERT INTO buytbl VALUES(NULL, 'BBK', '메모리', '전자', 80,  10);
INSERT INTO buytbl VALUES(NULL, 'SSK', '책'    , '서적', 15,   5);
INSERT INTO buytbl VALUES(NULL, 'EJW', '책'    , '서적', 15,   2);
INSERT INTO buytbl VALUES(NULL, 'EJW', '청바지', '의류', 50,   1);
INSERT INTO buytbl VALUES(NULL, 'BBK', '운동화', NULL   , 30,   2);
INSERT INTO buytbl VALUES(NULL, 'EJW', '책'    , '서적', 15,   1);
INSERT INTO buytbl VALUES(NULL, 'BBK', '운동화', NULL   , 30,   2);

SELECT * FROM usertbl;
SELECT * FROM buytbl;

# 2021-11-15

USE sqldb;

# ANY, ALL, IN
SELECT * FROM usertbl WHERE height > ALL (SELECT height FROM usertbl WHERE addr='경기'); -- 172보다 큰 결과
SELECT * FROM usertbl WHERE height < ALL (SELECT height FROM usertbl WHERE addr='경기'); -- 166보다 작은 결과

SELECT * FROM usertbl WHERE height > ANY (SELECT height FROM usertbl WHERE addr='경기'); -- 166보다 큰 결과
SELECT * FROM usertbl WHERE height < ANY (SELECT height FROM usertbl WHERE addr='경기'); -- 172보다 작은 결과

# DB 다시 생성
DROP DATABASE IF EXISTS sqldb; -- 만약 sqldb가 존재하면 우선 삭제한다.
CREATE DATABASE sqldb;
USE sqldb;
CREATE TABLE usertbl -- 회원 테이블
( userID  	CHAR(8) NOT NULL PRIMARY KEY, -- 사용자 아이디(PK)
  name    	VARCHAR(10) NOT NULL, -- 이름
  birthYear   INT NOT NULL,  -- 출생년도
  addr	  	CHAR(2) NOT NULL, -- 지역(경기,서울,경남 식으로 2글자만입력)
  mobile1	CHAR(3), -- 휴대폰의 국번(011, 016, 017, 018, 019, 010 등)
  mobile2	CHAR(8), -- 휴대폰의 나머지 전화번호(하이픈제외)
  height    	SMALLINT,  -- 키
  mDate    	DATE  -- 회원 가입일
);
CREATE TABLE buytbl -- 회원 구매 테이블(Buy Table의 약자)
(  num 		INT AUTO_INCREMENT NOT NULL PRIMARY KEY, -- 순번(PK)
   userID  	CHAR(8) NOT NULL, -- 아이디(FK)
   prodName 	CHAR(6) NOT NULL, --  물품명
   groupName 	CHAR(4)  , -- 분류
   price     	INT  NOT NULL, -- 단가
   amount    	SMALLINT  NOT NULL, -- 수량
   FOREIGN KEY (userID) REFERENCES usertbl(userID)
);

INSERT INTO usertbl VALUES('LSG', '이승기', 1987, '서울', '011', '1111111', 182, '2008-8-8');
INSERT INTO usertbl VALUES('KKH', '김경호', 1971, '전남', '019', '3333333', 177, '2007-7-7');
INSERT INTO usertbl VALUES('KBS', '김범수', 1979, '경남', null, null, null, null);
INSERT INTO usertbl VALUES('JYP', '조용필', 1950, '경기', null, null, null, null);
INSERT INTO usertbl(userID, name, birthYear, addr) VALUES('LJB', '임재범', 1963, '서울');
INSERT INTO usertbl(userID, name, birthYear, addr, height, mDate) VALUES('SSK', '성시경', 1979, '서울', 186, '2013-12-12');

SELECT * FROM usertbl;

INSERT INTO buytbl(num, userId, prodName, groupName, price, amount)
VALUES(NULL, 'KBS', '운동화', NULL   , 30,   2);
INSERT INTO buytbl(userId, prodName, groupName, price, amount)
VALUES('KBS', '노트북', '전자', 1000, 1);

# 외래키의 제약 조건이 RESTRICT로 되어 있기 때문에 아래의 항목은 삽입할 수 없다.
# INSERT INTO buytbl VALUES(NULL, 'BBK', '모니터', '전자', 200,  5);

SELECT * FROM buytbl;

SELECT last_insert_id();

# AUTO_INCREMENT는 반드시 하나의 컬럼에만 지정가능하고, 해당 컬럼은 유일값으로 설정되어야 한다.
# 따라서 아래 구문은 실행 불가능
-- CREATE TABLE testtbl
-- (
-- 	num int AUTO_INCREMENT,
--     name varchar(20)
-- );

CREATE TABLE testtbl
(
	num int AUTO_INCREMENT PRIMARY key,
    name varchar(20)
);
# or
CREATE TABLE testtbl2
(
	num int AUTO_INCREMENT UNIQUE,
    name varchar(20)
);

INSERT INTO testtbl(num, name) VALUES(NULL, 'kim');
INSERT INTO testtbl(num, name) VALUES(NULL, 'min');
SELECT * FROM testtbl;

INSERT INTO testtbl2(num, name) VALUES(NULL, '김');
SELECT * FROM testtbl2;
SELECT last_insert_id();

INSERT INTO testtbl(num, name) VALUES(NULL, 'park');
SELECT last_insert_id();

# AUTO_INCREMENT 값 변경
ALTER TABLE testtbl AUTO_INCREMENT=100;
INSERT INTO testtbl(num, name) VALUES(NULL, 'yoon');
SELECT * FROM testtbl;

# AUTO_INCREMENT 층가값 변경?
SET @@auto_increment_increment = 2;
INSERT INTO testtbl(num, name) VALUES(NULL, 'hong'); # 100 -> 101
SELECT * FROM testtbl;

SET @@auto_increment_increment = 3;
INSERT INTO testtbl(num, name) VALUES(NULL, 'dong'); # 103
SELECT * FROM testtbl;

ALTER TABLE testtbl AUTO_INCREMENT=200;
SET @@auto_increment_increment = 2;
INSERT INTO testtbl(num, name) VALUES(NULL, 'woo'); # 201
SELECT * FROM testtbl;

INSERT INTO testtbl(num, name) VALUES(NULL, 'pko'); # 203
SELECT * FROM testtbl;

INSERT INTO testtbl(num, name) VALUES(NULL, 'wkk'); # 205
SELECT * FROM testtbl;

ALTER TABLE testtbl AUTO_INCREMENT=300;
SET @@auto_increment_increment = 3;
INSERT INTO testtbl(num, name) VALUES(NULL, 'woo'); # 201
SELECT * FROM testtbl;

INSERT INTO testtbl(num, name) VALUES(NULL, 'pko'); # 203
SELECT * FROM testtbl;

INSERT INTO testtbl(num, name) VALUES(NULL, 'wkk'); # 205
SELECT * FROM testtbl;

UPDATE testtbl SET name='ami' WHERE num = 1;
SELECT * FROM testtbl;

UPDATE usertbl SET mobile1='011', mobile2='7777777' WHERE userID = 'SSK';
SELECT * FROM usertbl;

DELETE FROM testtbl WHERE num = 1;
SELECT * FROM testtbl;

# DELETE 문에서도 LIMIT를 사용 가능
SELECT * FROM testtbl;
DELETE FROM testtbl WHERE num < 5 LIMIT 3; # 2 3이 사라짐
SELECT * FROM testtbl;

SELECT * FROM testtbl;
DELETE FROM testtbl WHERE num < 200 LIMIT 2; # 100 101 103이 사라짐
SELECT * FROM testtbl;

SELECT * FROM testtbl;
DELETE FROM testtbl WHERE num < 304 LIMIT 2;
SELECT * FROM testtbl;

# UPDATE문에서도 LIMIT을 사용할 수 있다.
UPDATE testtbl SET name='홍길동' LIMIT 1;
UPDATE testtbl SET name='홍길동' WHERE name LIKE '%o%' LIMIT 1;
SELECT * FROM testtbl;

CREATE TABLE tdata1 (SELECT * FROM testtbl);
CREATE TABLE tdata2 (SELECT * FROM testtbl);
CREATE TABLE tdata3 (SELECT * FROM testtbl);

DELETE FROM tdata1;
TRUNCATE tdata2;
DROP TABLE tdata3;

CREATE TABLE mem1(
	num int PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(20)
);

CREATE TABLE mem2(
	num int UNIQUE AUTO_INCREMENT,
    name VARCHAR(20),
    PRIMARY KEY(num)
);

CREATE TABLE mem3(
	num int UNIQUE AUTO_INCREMENT,
    name VARCHAR(20)
);
ALTER TABLE mem3
	ADD CONSTRAINT mem3_num_pk PRIMARY KEY(num);

#비재귀적 CTE
WITH cte(userID, cnt)
AS
(
	SELECT userID, price*amount FROM buytbl
)
SELECT * FROM cte;

SET @myVal1 = 5;
SET @myVal2 = 10;
SELECT @myVal1, @myVal2;

SELECT concat('고객 이름=>', name) FROM usertbl;

# 동적 쿼리
PREPARE myData
	FROM 'SELECT * FROM usertbl LIMIT ?';
EXECUTE myData USING @myVal1;

# ifnull
# NULL이 아니면 원래값 반환, NULL이면 지정한 값 반환
SELECT ifnull(NULL, '홍길동');
SELECT ifnull('이영희', '홍길동');

# nullif
# 참이면 null, 거짓이면 첫번째 인자
SELECT nullif(100, 100);
SELECT nullif(100, 200);

# 2021-11-16

USE sqldb;

CREATE TABLE pivotTest (
	uName CHAR(3),
    season CHAR(2),
    amount INT
);

INSERT  INTO  pivotTest VALUES
	('김범수' , '겨울',  10) , ('윤종신' , '여름',  15) , ('김범수' , '가을',  25) , ('김범수' , '봄',    3) ,
	('김범수' , '봄',    37) , ('윤종신' , '겨울',  40) , ('김범수' , '여름',  14) ,('김범수' , '겨울',  22) ,
	('윤종신' , '여름',  64) ;

SELECT * FROM pivotTest;

# GROUP_CONCAT
SELECT uName, GROUP_CONCAT(season SEPARATOR '/') FROM pivotTest GROUP BY uName;

# JSON 관련 함수 연습
SELECT JSON_OBJECT('name', name, 'height', height) AS `JSON 값` FROM usertbl WHERE height >= 180;

SET @json='{"usertbl":
[
{"name": "임재범", "height": 182},
{"name": "이승기", "height": 182},
{"name": "성시경", "height": 186}
]
}';
SELECT JSON_VALID(@json) AS JSON_VALID;
SELECT JSON_SEARCH(@json, 'one', '성시경') AS JSON_SEARCH;
SELECT JSON_EXTRACT(@json, '$.usertbl[2].name') AS JSON_EXTRACT;
SELECT JSON_INSERT(@json, '$.usertbl[0].mDate', '2009-09-09') AS JSON_INSERT;
SELECT JSON_REPLACE(@json, '$.usertbl[0].name', '홍길동') AS JSON_REPLACE;
SELECT JSON_REMOVE(@json, '$.usertbl[0]') AS JSON_REMOVE;

CREATE TABLE stdtbl 
( stdName    VARCHAR(10) NOT NULL PRIMARY KEY,
  addr	  CHAR(4) NOT NULL
);
CREATE TABLE clubtbl 
( clubName    VARCHAR(10) NOT NULL PRIMARY KEY,
  roomNo    CHAR(4) NOT NULL
);
CREATE TABLE stdclubtbl
(  num int AUTO_INCREMENT NOT NULL PRIMARY KEY, 
   stdName    VARCHAR(10) NOT NULL,
   clubName    VARCHAR(10) NOT NULL,
FOREIGN KEY(stdName) REFERENCES stdtbl(stdName),
FOREIGN KEY(clubName) REFERENCES clubtbl(clubName)
);
INSERT INTO stdtbl VALUES ('김범수','경남'), ('성시경','서울'), ('조용필','경기'), ('은지원','경북'),('바비킴','서울');
INSERT INTO clubtbl VALUES ('수영','101호'), ('바둑','102호'), ('축구','103호'), ('봉사','104호');
INSERT INTO stdclubtbl VALUES (NULL, '김범수','바둑'), (NULL,'김범수','축구'), (NULL,'조용필','축구'), (NULL,'은지원','축구'), (NULL,'은지원','봉사'), (NULL,'바비킴','봉사');

# jdbc 시연용 producttbl, customertbl
CREATE TABLE `producttbl` (
  `pcode` char(4) NOT NULL,
  `company` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`pcode`)
);

INSERT INTO `sqldb`.`producttbl` (`pcode`, `company`) VALUES ('G001', 'Nikon');
INSERT INTO `sqldb`.`producttbl` (`pcode`, `company`) VALUES ('G002', 'Sony');
INSERT INTO `sqldb`.`producttbl` (`pcode`, `company`) VALUES ('G003', 'FujiFilm');

CREATE TABLE `sqldb`.`customertbl` (
  `code` CHAR(4) NOT NULL,
  `name` VARCHAR(10) NULL,
  `price` INT NULL,
  `product` CHAR(10) NULL,
  PRIMARY KEY (`code`)
);

INSERT INTO `sqldb`.`customertbl` (`code`, `name`, `price`, `product`) VALUES ('A001', '홍길동', '15000', 'Nikon');
INSERT INTO `sqldb`.`customertbl` (`code`, `name`, `price`, `product`) VALUES ('A002', '이영희', '25000', 'Sony');
INSERT INTO `sqldb`.`customertbl` (`code`, `name`, `price`, `product`) VALUES ('A003', '박달재', '18000', 'FujiFilm');

# @@autocommit값 확인
SELECT @@autocommit;
SET @@autocommit = 0;
SELECT @@autocommit;

INSERT INTO `sqldb`.`customertbl` (`code`, `name`, `price`, `product`) VALUES ('A004', '이건희', '11111', 'Samsung');
SELECT * FROM customertbl;
ROLLBACK;

SET @@autocommit = 1;
SELECT @@autocommit;
