# 2021-11-11

CREATE SCHEMA `shopdb` ;

USE shopdb;

CREATE TABLE `memberTBL` (
  `memberID` char(8) NOT NULL,
  `memberName` char(5) NOT NULL,
  `memberAddress` char(20) DEFAULT NULL,
  PRIMARY KEY (`memberID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `productTBL` (
  `productName` char(4) NOT NULL,
  `cost` int NOT NULL,
  `makeDate` date DEFAULT NULL,
  `company` char(5) DEFAULT NULL,
  `amount` int NOT NULL,
  PRIMARY KEY (`productName`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO shopdb.memberTBL(memberID, memberName, memberAddress) VALUES
('Dang', '당탕이', '경기 부천시 중동'),
('Han', '한주연', '인천 남구 주안동'),
('Jee', '지운이', '서울 은평구 증산동'),
('Sang', '상길이', '경기 성남시 분당구');

INSERT INTO shopdb.productTBL(productName, cost, makeDate, company, amount) VALUES
('냉장고', 5, '2023-02-01', '대우', 22),
('세탁기', 20, '2022-09-01', 'LG', 3),
('컴퓨터', 10, '2021-01-01', '삼성', 17);

SELECT * FROM productTBL WHERE productName='세탁기'; # 인덱스가 없는 상태에서는 테이블 전체를 검색함
CREATE INDEX idx_productTBL_productName ON productTBL(productName);
SELECT * FROM productTBL WHERE productName='세탁기'; # 인덱스를 만들고 나면 인덱스를 통해 테이블을 일부만 탐색함

CREATE VIEW uv_memberTBL
AS
	SELECT memberName, memberAddress FROM memberTBL;
SELECT * FROM uv_memberTBL;

DELIMITER $$
CREATE PROCEDURE `myProc` ()
BEGIN
	SELECT * FROM memberTBL WHERE memberName="당탕이";
    SELECT * FROM productTBL WHERE productName="냉장고";
END$$
DELIMITER ;
CALL myProc();

INSERT INTO memberTBL VALUES ('Figure', '연아', '경기도 군포시 당정동');
SELECT * FROM memberTBL;

UPDATE memberTBL SET memberAddress = '서울 강남구 역삼동' WHERE memberName = '연아';
SELECT * FROM memberTBL;

DELETE FROM memberTBL WHERE memberName = '연아';
SELECT * FROM memberTBL;

# 회원 데이터를 바로 삭제해버리면 해당 사용자가 언제까지 회원이였는지 알수가 없다.
# 따라서 회원 데이터와 같이 탈퇴 후에도 일정 기간동안은 데이터를 가지고 있어야 하는 경우에
# 별도의 테이블을 생성하거나 플래그를 넣는 등으로 해결한다.
CREATE TABLE deletedMemberTBL (
	memberID char(8),
    memberName char(5),
    memberAddress char(20),
    deletedDate date -- 삭제한 날짜
);

DELIMITER $$
CREATE 
TRIGGER shopdb.trg_deletedMemberTBL
	AFTER DELETE
	ON shopdb.memberTBL
	FOR EACH ROW
BEGIN
  -- OLD 테이블의 내용을 백업 테이블에 삽입
  INSERT INTO deletedMemberTBL
    VALUES (OLD.memberID, OLD.memberName, OLD.memberAddress, CURDATE());
END$$
DELIMITER ;

DELETE FROM memberTBL WHERE memberName = '당탕이';
SELECT * FROM deletedMemberTBL;

-----------------------------
# 2021-11-12

