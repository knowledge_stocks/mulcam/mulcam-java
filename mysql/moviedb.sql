# 2021-11-15

CREATE DATABASE moviedb;

USE moviedb;
CREATE TABLE movietbl 
  (movie_id        INT,
   movie_title     VARCHAR(30),
   movie_director  VARCHAR(20),
   movie_star      VARCHAR(20),
   movie_script    LONGTEXT,
   movie_film      LONGBLOB
) DEFAULT CHARSET=utf8mb4;

# DB 서버에서 아래 내용 설정하고, 파일을 서버에 놓고 실행해야 한다.
# max_allowed_packet=1024M
# secure-file-priv
# 나는 귀찮아서 걍 dbForge로 넣음
INSERT INTO movietbl VALUES (
	1, '쉰들러 리스트', '스필버그', '리암 니슨',
	load_file('D:/Sync/Repos/workspace/Multicampus/mysql/movies/Schindler.txt'),
    load_file('D:/Sync/Repos/workspace/Multicampus/mysql/movies/Schindler.mp4')
);
