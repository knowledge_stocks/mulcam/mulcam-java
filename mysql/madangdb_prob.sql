
DROP DATABASE IF EXISTS  madangdb;
CREATE DATABASE IF NOT EXISTS madangdb; 
USE madangdb;

CREATE TABLE Book (
  bookid      INTEGER PRIMARY KEY,
  bookname    VARCHAR(40),
  publisher   VARCHAR(40),
  price       INTEGER 
);

CREATE TABLE  Customer (
  custid      INTEGER PRIMARY KEY,  
  name        VARCHAR(40),
  address     VARCHAR(50),
  phone       VARCHAR(20)
);

CREATE TABLE Orders (
  orderid INTEGER PRIMARY KEY,
  custid  INTEGER ,
  bookid  INTEGER ,
  saleprice INTEGER ,
  orderdate DATE,
  FOREIGN KEY (custid) REFERENCES Customer(custid),
  FOREIGN KEY (bookid) REFERENCES Book(bookid)
);


CREATE TABLE Imported_Book (
  bookid      INTEGER,
  bookname    VARCHAR(40),
  publisher   VARCHAR(40),
  price       INTEGER 
);

INSERT INTO Book VALUES(1, '축구의 역사', '굿스포츠', 7000);
INSERT INTO Book VALUES(2, '축구아는 여자', '나무수', 13000);
INSERT INTO Book VALUES(3, '축구의 이해', '대한미디어', 22000);
INSERT INTO Book VALUES(4, '골프 바이블', '대한미디어', 35000);
INSERT INTO Book VALUES(5, '피겨 교본', '굿스포츠', 8000);
INSERT INTO Book VALUES(6, '역도 단계별기술', '굿스포츠', 6000);
INSERT INTO Book VALUES(7, '야구의 추억', '이상미디어', 20000);
INSERT INTO Book VALUES(8, '야구를 부탁해', '이상미디어', 13000);
INSERT INTO Book VALUES(9, '올림픽 이야기', '삼성당', 7500);
INSERT INTO Book VALUES(10, 'Olympic Champions', 'Pearson', 13000);

INSERT INTO Customer VALUES (1, '박지성', '영국 맨체스타', '000-5000-0001');
INSERT INTO Customer VALUES (2, '김연아', '대한민국 서울', '000-6000-0001');  
INSERT INTO Customer VALUES (3, '장미란', '대한민국 강원도', '000-7000-0001');
INSERT INTO Customer VALUES (4, '추신수', '미국 클리블랜드', '000-8000-0001');
INSERT INTO Customer VALUES (5, '박세리', '대한민국 대전',  NULL);

INSERT INTO Orders VALUES (1, 1, 1, 6000, STR_TO_DATE('2014-07-01','%Y-%m-%d')); 
INSERT INTO Orders VALUES (2, 1, 3, 21000, STR_TO_DATE('2014-07-03','%Y-%m-%d'));
INSERT INTO Orders VALUES (3, 2, 5, 8000, STR_TO_DATE('2014-07-03','%Y-%m-%d')); 
INSERT INTO Orders VALUES (4, 3, 6, 6000, STR_TO_DATE('2014-07-04','%Y-%m-%d')); 
INSERT INTO Orders VALUES (5, 4, 7, 20000, STR_TO_DATE('2014-07-05','%Y-%m-%d'));
INSERT INTO Orders VALUES (6, 1, 2, 12000, STR_TO_DATE('2014-07-07','%Y-%m-%d'));
INSERT INTO Orders VALUES (7, 4, 8, 13000, STR_TO_DATE( '2014-07-07','%Y-%m-%d'));
INSERT INTO Orders VALUES (8, 3, 10, 12000, STR_TO_DATE('2014-07-08','%Y-%m-%d')); 
INSERT INTO Orders VALUES (9, 2, 10, 7000, STR_TO_DATE('2014-07-09','%Y-%m-%d')); 
INSERT INTO Orders VALUES (10, 3, 8, 13000, STR_TO_DATE('2014-07-10','%Y-%m-%d'));

INSERT INTO Imported_Book VALUES(21, 'Zen Golf', 'Pearson', 12000);
INSERT INTO Imported_Book VALUES(22, 'Soccer Skills', 'Human Kinetics', 15000);


-- 1.  도서번호가  1인  도서의  이름을 검색하세요. 
SELECT bookname FROM Book WHERE bookid = 1;

-- 2.  가격이  20,000원  이상인  도서의  이름을 검색하세요.
SELECT bookname FROM Book WHERE price >= 20000;

-- 3. 박지성의    총    구매액을 검색하세요.
SELECT
	SUM(`Orders`.saleprice)
FROM
	Customer, `Orders`
WHERE
	Customer.custid = `Orders`.custid
    AND Customer.name = '박지성';

-- 4. 박지성이    구매한    도서의    수를 검색하세요. 
SELECT
	COUNT(*)
FROM
	Customer, `Orders`
WHERE
	Customer.name = '박지성'
	AND Customer.custid = `Orders`.custid;

-- 5. 박지성이    구매한    도서의    출판사    수를 검색하세요.
SELECT
	COUNT(DISTINCT Book.publisher)
FROM
	Customer, `Orders`, Book
WHERE
	Customer.name = '박지성'
	AND Customer.custid = `Orders`.custid
    AND `Orders`.bookid = Book.bookid;

-- 6. 박지성이    구매한    도서의    이름,    가격,    정가와    판매가격의    차이를 검색하세요.
SELECT
	Book.bookname,
    Book.price,
    Book.price - `Orders`.saleprice AS `할인`
FROM
	Customer, `Orders`, Book
WHERE
	Customer.name = '박지성'
	AND Customer.custid = `Orders`.custid
    AND `Orders`.bookid = Book.bookid;


-- 7.  박지성이    구매하지    않은    도서의    이름을 검색하세요.
SELECT
	Book.bookname
FROM
	Customer LEFT JOIN `Orders`
ON
	Customer.name = '박지성'
	AND Customer.custid = `Orders`.custid
RIGHT JOIN Book
ON
	`Orders`.bookid = Book.bookid
WHERE
	`Orders`.custid IS NULL;



-- 8.  마당서점    도서의    총    개수를 검색하세요.
SELECT
	COUNT(*)
FROM Book;

-- 9.     마당서점에    도서를    출고하는    출판사의    총    개수를 검색하세요.
SELECT
	COUNT(DISTINCT publisher)
FROM Book;

--  10. 모든  고객의  이름,  주소를  검색하세요.
SELECT
	name,
    address
FROM Customer;

-- 11.  2014년    7월    4일  ~ 7월   7일  사이에  주문받은 도서의 주문번호를 검색하세요.
SELECT
	orderid
FROM Orders
WHERE
	orderdate >= '2014-07-04'
    AND orderdate < '2014-07-07';

-- 12.  2014년    7월    4일 ~ 7월  7일  사이에 주문받은 도서를 제외한 도서의 주문번호를 검색하세요.
SELECT
	orderid
FROM Orders
WHERE
	NOT(orderdate >= '2014-07-04'
    AND orderdate < '2014-07-07');


-- 13.  성이    '김'  씨인 고객의 이름과  주소를 검색하세요.
SELECT
	name,
    address
FROM Customer
WHERE name LIKE '김%';


-- 14.  성이   '김' 씨이고   이름이   '아' 로 끝나는 고객의  이름과  주소를 검색하세요. 
SELECT
	name,
    address
FROM Customer
WHERE
	name LIKE '김%'
    AND name LIKE '%아';

-- 15. 주문하지  않은  고객의  이름을 검색하세요.  (서브쿼리 사용)  
SELECT
	name
FROM Customer
WHERE
	NOT EXISTS
    (
		SELECT *
        FROM Orders
        WHERE Customer.custid = Orders.custid
	);

-- 16.  주문  금액의  총액과  주문의 평균  금액을 검색하세요.
SELECT
	SUM(saleprice) 총액,
    AVG(saleprice) `평균 금액`
FROM Orders;
 
-- 17.  고객의    이름과    고객별    구매액을 검색하세요.
SELECT
	Customer.name,
	SUM(Orders.saleprice) 구매액
FROM Orders, Customer
WHERE
	Orders.custid = Customer.custid
GROUP BY
	Orders.custid;

-- 18.   고객의    이름과    고객이    구매한    도서    목록을 검색하세요.
SELECT
	Customer.name,
	Book.bookname
FROM Orders, Customer, Book
WHERE
	Orders.custid = Customer.custid
    AND Orders.bookid = Book.bookid;


-- 19.  도서의    가격(Book 테이블)과 판매가격 (Orders    테이블) 의    차이가    가장  많은  주문을 검색하세요.      
SELECT
	Orders.*,
	Book.price - Orders.saleprice
FROM Book, Orders
WHERE Book.bookid = Orders.bookid
ORDER BY (Book.price - Orders.saleprice) DESC
LIMIT 1;

# 강사의 풀이
SELECT
	*
FROM Book, Orders
WHERE Book.bookid = Orders.bookid
	AND Book.price - Orders.saleprice = (
		SELECT MAX(Book.price - Orders.saleprice)
        FROM Book, Orders
        WHERE Book.bookid = Orders.bookid);
        

-- 20. 도서의    판매액    평균보다    자신의    구매액    평균이    더    높은    고객의    이름을 검색하세요.
SELECT
	Customer.name
FROM
(
	SELECT
		custid,
		AVG(saleprice) AS 구매액
	FROM Orders
	GROUP BY custid
) AS `구매`
JOIN
(
	SELECT
		AVG(saleprice) `평균 판매액`
	FROM Orders
) AS `판매`
ON `구매`.구매액 > `판매`.`평균 판매액`
JOIN Customer
ON Customer.custid = `구매`.custid;
	
# 강사의 풀이
SELECT
	name, AVG(saleprice)
FROM Customer, Orders
WHERE Customer.custid = Orders.custid
GROUP BY Customer.name
HAVING AVG(saleprice) > (SELECT AVG(saleprice) FROM Orders);

-- 21.  박지성이    구매한    도서의    출판사와    같은    출판사에서    도서를    구매한    고객의    이름을 검색하세요.
SELECT
	DISTINCT Customer.name
FROM Book
JOIN
(
	SELECT
		DISTINCT Book.publisher,
		Customer.custid
	FROM
		Customer, `Orders`, Book
	WHERE
		Customer.name = '박지성'
		AND Customer.custid = `Orders`.custid
		AND `Orders`.bookid = Book.bookid
) AS `박지성의 출판사`
ON Book.publisher = `박지성의 출판사`.publisher
JOIN Orders
ON Orders.bookid = Book.bookid
JOIN Customer
ON Customer.custid = Orders.custid
AND Orders.custid != `박지성의 출판사`.custid;

# 강사의 풀이. 이름이 겹치는 유저가 있을 수 있어서 문제가 있을거 같긴한데.. 생각해보니 내 풀이도 마찬가지네
SELECT
	name, publisher
FROM Customer, Orders, Book
WHERE Customer.custid = Orders.custid
	AND Orders.bookid = Book.bookid
    AND name != '박지성'
    AND publisher IN
		(
			SELECT
				DISTINCT Book.publisher
			FROM
				Customer, `Orders`, Book
			WHERE
				Customer.name = '박지성'
				AND Customer.custid = `Orders`.custid
				AND `Orders`.bookid = Book.bookid
		);

-- 22.   두  개   이상의  서로    다른    출판사에서    도서를    구매한    고객의    이름을 검색하세요.
SELECT
	name
FROM
(
	SELECT
		DISTINCT Book.publisher, Customer.custid, Customer.name
	FROM
		Customer, `Orders`, Book
	WHERE
		Customer.custid = `Orders`.custid
		AND `Orders`.bookid = Book.bookid
) AS `고객별 출판사`
GROUP BY custid
HAVING COUNT(*) >= 2;

# 강사의 풀이
SELECT
	name
FROM Customer c1
WHERE 2 <= (
	SELECT COUNT(DISTINCT publisher)
    FROM Customer, Orders, Book
    WHERE Customer.custid = Orders.custid
		AND Orders.bookid=Book.bookid
        AND Customer.name=c1.name
	);


-- 23.    전체    고객의    30%    이상이    구매한    도서를 검색하세요. 
# 책 이름 표기는 귀찮다..
SELECT
	bookid,
	count(*) AS '구매 고객 수'
FROM
(
	SELECT
		Orders.bookid, Customer.custid
	FROM Orders, Customer
	WHERE Orders.custid = Customer.custid
	GROUP BY Orders.bookid, Customer.custid
) AS `고객별 구매서적`
GROUP BY bookid
HAVING (COUNT(*) / (SELECT COUNT(*) FROM Customer)) >= 0.3;

# 강사의 풀이, 동일한 사람이 같은 서적을 여러번 구매한 것은 처리하지 않음
SELECT
	bookname
From Book b1
WHERE (
	SELECT COUNT(Book.bookid)
    FROM Book, Orders
    WHERE Book.bookid = Orders.bookid
		AND Book.bookid = b1.bookid
	) >= 0.3 * (SELECT COUNT(*) FROM Customer);

